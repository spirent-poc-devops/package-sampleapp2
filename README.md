# <img src="https://upload.wikimedia.org/wikipedia/en/thumb/c/ce/Spirent_logo.svg/490px-Spirent_logo.svg.png" alt="Spirent Logo" width="200"> <br/> SampleApp Deployment Using AppDeploy Scripts

Sample application deployment package that uses shared [AppDeploy Scripts](https://gitlab.com/spirent-poc-devops/script-appdeploy-ps)

# Use

Before installing the application you shall provision an environment using one of the available provisioning scripts:
* [Development Environment](https://gitlab.com/spirent-poc-devops/env-dev-ps)
* [Test Environment](https://gitlab.com/spirent-poc-devops/env-test-ps)
* [On-Premises Environment](https://gitlab.com/spirent-poc-devops/env-onprem-ps)
* [Cloud AWS Environment](https://gitlab.com/spirent-poc-devops/env-cloudaws-ps)

To install the application
```bash
<path to appdeploy>/install_app.ps1 -Manifest manifest.json -Config <path to env config>
```

To upgrade the application
```bash
<path to appdeploy>/upgrade_app.ps1 -Manifest manifest.json -Config <path to env config>
```

To uninstall the application
```bash
<path to appdeploy>/uninstall_app.ps1 -Manifest manifest.json -Config <path to env config>
```

To generate a new baseline by tagging container in a target environment
```bash
<path to appdeploy>/baseline_app.ps1 -Manifest manifest.json -Config <path to env config> -Version <version>
```

# Contacts

This sample was created by *Sergey Seroukhov*